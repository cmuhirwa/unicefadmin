<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use App\Charts\SampleChart;
use App\Charts;
use App\answer;
use App\answersview;
use App\option;
use App\Student;
use App\User;
use App\closed;
use Datatables;

class AjaxController extends Controller {
   public function page_blank()
    {
      return view('pages.page_blank');
   }


   public function readData()
   {

    $id=$_GET['id'];
    $answersview=answersview::all() ->where('questionsCode', '=', $id);
       return view('pages.service')->with('answersview',$answersview);
   }

 
    function fetchdata(Request $request)
    {
        $id = $request->input('id');
        $student = Student::find($id);
        $output = array(
            'first_name'    =>  $student->first_name,
            'last_name'     =>  $student->last_name
        );
        echo json_encode($output);
    }

    function removedata(Request $request)
    {
        $student = Student::find($request->input('id'));
        if($student->delete())
        {
            echo 'Data Deleted';
        }
    }


   }
   // $services =  DB::table('closeds')
   //       ->join('services','services.id','closeds.id')
   //          ->select('closeds.qoption','services.tab','services.boys','services.girls')
   //      ->get();
// $options = option::all()->where('questionsCode', '=', $id);
//     $answers = answer::all()->where('optionCode', '=', $id);

 