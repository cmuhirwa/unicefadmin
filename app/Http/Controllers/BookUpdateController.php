<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\book;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
// use App\Http\Controllers\Controller;


class BookUpdateController extends Controller {

   public function index(){
      
   }

   public function store(Request $request, $id)
   {
       $cover = $request->file('file1');
    $extension = $cover->getClientOriginalExtension();
    Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));

      $ubooks = book::find($id);

      $ubooks->text = $request->input('text1');
      $ubooks->file = $cover->getFilename().'.'.$extension;
      // $ubooks->file = $request->input('file1');
      $ubooks->save();
        return redirect('/books')->with('message', 'question added ');
      // return response()->json($ubooks);
        
   }

   public function update()
   {

   
   }
   
}
