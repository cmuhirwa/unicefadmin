<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use DB;
use Input;
use App\book;
use App\Http\Controllers\Controller;

class BooksController extends Controller
{
     public function product(){
     	 $tbooks = book::all();
    return view('pages.books')->with('tbooks',$tbooks);
       // return response()->json($tbooks);
  }

   public function insert(Request $request)
   {

     $cover = $request->file('file');
    $extension = $cover->getClientOriginalExtension();
    Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));


   	$tbook = new book();
       $tbook->text = $request->text;
       $tbook->file = $cover->getFilename().'.'.$extension;
       // $tbook->file = $request->file;
       $tbook->save();  
      return redirect('/books')->with('message','yes');
       // return response()->json($tbook);
   }
    
    function fetchdata(Request $request)
    {
        $id = $request->input('id');
        $fbooks = book::find($id);
        $output = array(
            'text'    =>  $fbooks->text,
            'file'     =>  $fbooks->file
        );
        echo json_encode($output);
    }
}


