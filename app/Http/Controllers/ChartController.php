<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Charts;
use App\Service;
use ConsoleTVs\Charts\Classes\Echarts\Chart;
use App\User;
use App\question;
use App\Charts\SampleChart;

class ChartController extends Controller
{

public function example()
{
    $services = Service::all();
     return view('pages.chat')->with('services',$services);
}
public function page_blank()
{
    $questions = question::all();
     return view('pages.chat')->with('questions',$questions);
}

    public function SessionAnswer()
    {
        // $id=$_GET['id'];
        $today_users = User::whereDate('created_at', today())->count();
$yesterday_users = User::whereDate('created_at', today()->subDays(1))->count();
$users_2_days_ago = User::whereDate('created_at', today()->subDays(2))->count();
$services = service::all('boys')->where('boys', '>=', 70)->count();
$services1 = service::all('tab')->count();
 $services2 = Service::all();
 $questions = question::all();

    $chart = new Chart;
    $chart->labels(['2 days ago', 'Yesterday', 'Today','Boys','Girls']);
     $chart->dataset('My dataset', 'bar', [$users_2_days_ago, $yesterday_users, $today_users,$services,$services1])
     ->options([
    'color' => '#39f']);
     $chart->height(400);
     $chart->width(0);
        return view('pages.chat')->with('chart',$chart)->with('services2',$services2);

    }
    /**
     * Initializes the chart.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     parent::__construct();
    // }


}
    
