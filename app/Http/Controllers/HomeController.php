<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function my_first_api()
    {
        $data = [
            'name' => 'MUGISHA',
            'mobile' => '0788677246',
            'email' => 'blessederic00@gmail.com'
        ];
        return response()->json($data);
    }
}
