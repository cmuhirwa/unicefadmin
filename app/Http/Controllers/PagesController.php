<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\questionsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\pages;
use App\Charts;
use App\answer;
use ConsoleTVs\Charts\Classes\Echarts\Chart;
use App\User;
use App\question;
use App\closed;
use App\option;
use App\book;
use DB;
use App\tablet;


class PagesController extends Controller
{

    public function index()
    {
        // $title = 'welcome to my appp ';
        return view('pages.index');

    }
   

    public function page_blank()
    {
      // return "hey";
        $questions = question::all();
        return view('pages.page_blank')->with('questions',$questions);
        // return response()->json($questions);
        
    }

    public function excel()
    {
      return Excel::download(new questionsExport, 'question.xlsx');
    }
      
    


    public function insert(Request $request)
   {
    $tablet = new tablet();
      $tablet->pin = $request->pin;
      $tablet->serial = $request->serial;
    $tablet->save();  
      return redirect('/dashboard')->with('message','yes');
       // return response()->json($tablet);
   }

  

   public function dashboard(){
    $questions = question::all();
    $tablets =tablet::all();
    $books =book::all();
    $closeds = option::all();
    $answers = answer::all();
    return view('pages.dashboard')->with('questions',$questions)->with('tablets',$tablets)->with('books',$books)
    ->with('closeds',$closeds)->with('answers',$answers);
   }


   // question table


   public function save(Request $request){
        // dd($request->all());
      $tbquestion = new question();
      
      $id = time();
      $tbquestion->id = $id;
      $tbquestion->question = $request->question;
      $tbquestion->questionType = $request->questionType;
      $optionNumber = $request->optionName;
      $i = 0;
      while ($i<=$optionNumber) {
        $tbclosed = new option();
        $tbclosed->questionsCode = $id;
        $tbclosed->optionName = $request->optionInput[$i];
     
        $i++;
         $tbclosed->save();
      }
      $tbquestion->save();
        

      return redirect('/page_blank')->with('message', 'question added ');
      // return response()->json($tbquestion,$tbclosed);

    }

     
     // closed table


    public function send(Request $request){
        // dd($request->all());
      $tbclosed = new question();

      $tbclosed->optionName = $request->optionName;
      $tbclosed->save();

      return redirect('/page_blank')->with('message', 'question added ');

    }
    



//      public function books(){
//        $tbbooks = book::all();
//     return view('pages.books')->with('books',$tbbooks);
//     $tbook = DB::select('select * from books');
// return view('pages.books',['book'=>$tbook]);
//    }

    public function destroy($id) 
   {
      echo '<script> location.replace(document.referrer);</script>';
      DB::delete('delete from questions where id = ?',[$id]);
    
   }
  //   public function destroy(Request $request, $id)

  // {
  //   $deletes = question::find($id);
  //   $deletes->delete();
  //   // return view('pages.books')->with('deletes',$deletes);
  //   return response()->json($deletes); 
  // }

     
 }