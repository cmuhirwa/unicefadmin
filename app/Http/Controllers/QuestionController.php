<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\question;

class QuestionController extends Controller
{
    public function question(){
    	return view('pages.question');
    }
    public function save(Request $request){

        // dd($request->all());

    	$tbquestion = new question();

    	$tbquestion->question = $request->question;
    	$tbquestion->questionType = $request->questionType;

    	$tbquestion->save();

    	return redirect('/page_blank')->with('message', 'question added ');

    }
    public function store(Request $request, $id)
   {
      // $this->validate($request,[
      //    'text1'  => 'required',
      //    'file1'  => 'required',
      // ]);
      $uquestion = question::find($id);

      $uquestion->question = $request->input('qname1');
      $uquestion->save();
        return redirect('/page_blank')->with('message', 'question added ');
   }


   public function Piechart()
    {
      $Data = array
                  (
                    "0" => array
                                    (
                                      "value" => 335,
                                      "name" => "Apple",
                                    ),
                    "1" => array
                                    (
                                      "value" => 310,
                                      "name" => "Orange",
                                    )
                                    ,
                    "2" => array
                                    (
                                      "value" => 234,
                                      "name" => "Grapes",
                                    )
                                    ,
                    "3" => array
                                    (
                                      "value" => 135,
                                      "name" => "Banana",
                                    )
                  );
        return view('piechart',['Data' => $Data]);
    }
   

}
