<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //Table name
    protected $table = 'services';
    //Primary key
    public $primarykey = 'sid';
    //timestamp
    public $timestamp = 'true';

}
