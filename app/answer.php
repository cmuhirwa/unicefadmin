<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class answer extends Model
{
     //Table name
    protected $table = 'answers';
    //Primary key
    public $primarykey = 'id';
    //timestamp
    public $timestamp = 'true';
}
