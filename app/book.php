<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book extends Model
{
     //Table name
    protected $table = 'books';
    //Primary key
    public $primarykey = 'id';
    //timestamp
    public $timestamp = 'true';
}
