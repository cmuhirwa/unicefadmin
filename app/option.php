<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class option extends Model
{
     //Table name
    protected $table = 'options';
    //Primary key
    public $primarykey = 'id';
    //timestamp
    public $timestamp = 'true';
}
