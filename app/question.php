<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
   //Table name
    protected $table = 'questions';
    //Primary key
    public $primarykey = 'id';
    //timestamp
    public $timestamp = 'true';
}
