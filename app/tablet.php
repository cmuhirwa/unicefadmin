<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tablet extends Model
{
   //Table name
    protected $table = 'tablets';
    //Primary key
    public $primarykey = 'id';
    //timestamp
    public $timestamp = 'true';
}
