<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('optionName');
            $table->integer('questionsCode');  
            $table->integer('createdBy')->default('1');         
            $table->integer('updatedBy');
            $table->enum('archive',['NO','YES'])->default('NO');         
            $table->integer('archivedBy');    
            $table->date('archivedDate');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
