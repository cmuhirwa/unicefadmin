<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->


<!-- Mirrored from altair_html.tzdthemes.com/ecommerce_products_grid.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Feb 2019 11:15:27 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{config('app.name','Data Collection')}}</title>
    <link rel="icon" type="image/png" href="assets/img/images.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/images.png" sizes="32x32">

    <!-- additional styles for plugins -->
    <!-- dropify -->
    <link rel="stylesheet" href="assets/skins/dropify/css/dropify.css">
<!-- metrics graphics (charts) -->
    <link rel="stylesheet" href="bower_components/metrics-graphics/dist/metricsgraphics.css">
    <!-- c3.js (charts) -->
    <link rel="stylesheet" href="bower_components/c3js-chart/c3.min.css">
    <!-- chartist -->
    <link rel="stylesheet" href="bower_components/chartist/dist/chartist.min.css">

    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
        <link rel="stylesheet" href="assets/css/ie.css" media="all">
    <![endif]-->

</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                                
              
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                     
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                           
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                        <li class="uk-width-1-2 uk-active"><a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>
                                        <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>
                                    </ul>
                                    <ul id="header_alerts" class="uk-switcher uk-margin">
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-cyan">lj</span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Culpa nihil quo.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Quia explicabo nulla dolorem a repellat sint dignissimos quidem ipsam ex sint.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_07_tn.png" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Natus tempore.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Et ea et commodi vel vitae delectus.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-light-green">bh</span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Cupiditate sed.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Quia accusamus recusandae praesentium dolores necessitatibus in excepturi amet at.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_02_tn.png" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Autem quis.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Qui ratione labore voluptas quidem omnis ea quibusdam.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_09_tn.png" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Vitae ratione.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Fuga commodi aliquid et sunt molestiae odit doloremque tempore perspiciatis.</span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                                            </div>
                                        </li>
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Et autem.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Qui est voluptatibus dolor et sunt.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Et fuga facilis.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Iste ut est nulla rerum suscipit tempore.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Ut odit.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Deleniti facere illo quae provident iure voluptas voluptas.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Occaecati aut.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Commodi itaque in beatae autem molestias.</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                       <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image" src="assets/img/avatars/mell.png" alt=""/></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                     <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a></li>
                                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form uk-autocomplete" data-uk-autocomplete="{source:'data/search_data.json'}">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
               
            </form>
        </div>
    </header><!-- main header end -->
    <!-- main sidebar -->
    <aside id="sidebar_main">
        
        <div class="sidebar_main_header">
            <div class="sidebar_logo">
                <a href="page_blank" class="sSidebar_hide sidebar_logo_large">
                    <img class="logo_regular" src="assets/img/unice.png" alt="" height="15" width="80"/>
                    <img class="logo_light" src="assets/img/logo_main_white.png" alt="" height="15" width="71"/>
                </a>
                <a href="index-2.html" class="sSidebar_show sidebar_logo_small">
                    <img class="logo_regular" src="assets/img/logo_main_small.png" alt="" height="32" width="32"/>
                    <img class="logo_light" src="assets/img/logo_main_small_light.png" alt="" height="32" width="32"/>
                </a>
            </div>
            
        </div>
        
       <div class="menu_section">
            <ul>
                <li title="Dashboard">
                    <a href="dashboard">
                        <span class="menu_icon"><i class="material-icons">&#xE85C</i></span>
                        <span class="menu_title">Dashboard</span>
                    </a>
                    
               </li>
                
              
                <li title="Pages">
                    <a href="page_blank">
                        <span class="menu_icon"><i class="material-icons">&#xE8D2;</i></span>
                        <span class="menu_title">Questions</span>
                    </a>
                </li>
                <li title="Pages">
                    <a href="books">
                        <span class="menu_icon"><i class="material-icons">&#xE24D;</i></span>
                        <span class="menu_title">Child protect message</span>
                    </a>
                </li>
                         
            </ul>
        </div>
    </aside><!-- main sidebar end -->

    <div id="page_content">
        <div id="page_content_inner">


   


            <h3 class="heading_a uk-margin-bottom">Child Protect Messages</h3>

            <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4 hierarchical_show" data-uk-grid="{gutter: 20, controls: '#products_sort'}" data-show-delay="280">
                @if(count($tbooks) > 0)
                @foreach($tbooks as $value)
                    <div data-product-name="Debitis et." id="child_book">
                        <div class="md-card md-card-hover-img">
                             <div class="md-card-head uk-text-center md-card-content">
                                <img class="md-card-head-img" src="{{url('uploads/'.$value->file)}}" alt="{{$value->file}}"/>
                             </div>
                             <div class="md-card-content">
                                <p id="context-text">
                                 {{$value->text}}
                                </p>
                                <a class="md-btn md-btn-primary edit" data-uk-modal="{target:'#modal_header_footer'+{{$value->id}}}" href="#" id="{{$value->id}}">Edit</a>
                                <div class="uk-modal" id="modal_header_footer{{$value->id}}">
                                    <div class="uk-modal-dialog" id="bookModal">
                                      <form method="post" action="{{action('BookUpdateController@store', $value->id)}}"  enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {{-- <input type="hidden" name="_method" value="PATCH" /> --}}
                                        {{-- {{ method_field('PUT') }} --}}
                                    {{-- {!! Form::open(['action'=>'BooksDeleteController@updateBook','method'=>'post','role'=>'form']) !!}  --}}
                                        <div class="uk-modal-header">
                                            <h3 class="uk-modal-title">Edit</h3>
                                        </div>
                                         
                                               
                                           <div class="uk-width-medium-2-2">
                                            <span id="form_output"></span>
                                             <div class="parsley-row">
                                              <label>Text:</label>
                                               <input type="text" class="md-input" name="text1" id="text{{$value->id}}" value="{{$value->text}}">
                                              </div><br>
                                            <div class="uk-width-1-2 image" id="image">  
                                             <div class="md-card">
                                                 <div class="md-card-content">
                                                    {{-- @if ("assets/img/books/{{$value->file}}")
                                                    <img src="{{ $value->file }}">
                                                    @else
                                                     <p>No image found</p>
                                                @endif --}}
                                                
                                                      <input type="file" id="file{{$value->id}}" class="dropify" name="file1" data-default-file="{{url('uploads/'.$value->file)}}" alt="{{$value->file}}"/>
                                                 </div>
                                             </div>
                                            </div>
                                          </div>  
                                          <div class="uk-modal-footer uk-text-right">
                                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                            <input type="submit" class="md-btn md-btn-flat md-btn-flat-primary" value="Update" />
                                          </div>
                                      </form>
                                    {{-- {!! Form::close() !!} --}}
                                    </div>
                                </div>
                               {{--  <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}"> --}}
                                {{-- <a class="md-btn md-btn-danger md-btn delete" href="javascript:;" onclick="removeBook({{$value->id}})">Remove</a>  --}}
                                {{-- delete/{{ $value->id }} --}}
                                  <a class="md-btn md-btn-danger md-btn" href='delete/{{ $value->id }}' onclick = "return confirm('Are you sure you need to remove Book?')">Remove</a> 
                                   <script type="text/javascript">
                                        function removeBook(bookId) {
                                            if (confirm("Are you sure?")) {
                                                $.ajax({   
                                                    // var id = $(this).attr('id');
                                                    
                                                    type: 'DELETE',
                                                    dataType: "JSON",
                                                    data: {
                                                        "id": bookId,
                                                    },
                                                    success: function ()
                                                    {
                                                     alert("data deleted"); 
                                                    }
                                                });
                                            }
                                            else {
                                                alert("Don't")
                                            }
                                        }
                                   </script>
                             </div> 
                        </div>
                    </div>


                @endforeach
                @endif
                            
             
               {{--  <div data-product-name="Magni qui.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 509.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Magni qui.
                                <span class="sub-heading">SKU: 103134</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Qui dicta.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 495.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Qui dicta.
                                <span class="sub-heading">SKU: 130742</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Corporis cupiditate.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 517.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Corporis cupiditate.
                                <span class="sub-heading">SKU: 178457</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Quidem cupiditate voluptatem.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 515.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_1.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Quidem cupiditate voluptatem.
                                <span class="sub-heading">SKU: 157495</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Itaque voluptas.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 451.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_2.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Itaque voluptas.
                                <span class="sub-heading">SKU: 104883</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Minima doloribus ea.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 558.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_3.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Minima doloribus ea.
                                <span class="sub-heading">SKU: 158523</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Nulla enim.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 573.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_2.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Nulla enim.
                                <span class="sub-heading">SKU: 154265</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Et cumque.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 486.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Et cumque.
                                <span class="sub-heading">SKU: 141919</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Ratione consequuntur voluptate.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 600.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_2.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Ratione consequuntur voluptate.
                                <span class="sub-heading">SKU: 161599</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Et autem nostrum.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 466.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Et autem nostrum.
                                <span class="sub-heading">SKU: 184208</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Quis porro illum.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 475.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_3.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Quis porro illum.
                                <span class="sub-heading">SKU: 123901</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Minima nisi.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 496.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_3.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Minima nisi.
                                <span class="sub-heading">SKU: 153986</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="In dolores.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 521.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                In dolores.
                                <span class="sub-heading">SKU: 198947</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Possimus veniam.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 560.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_3.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Possimus veniam.
                                <span class="sub-heading">SKU: 142639</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div>
                <div data-product-name="Qui possimus nostrum.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 492.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_2.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Qui possimus nostrum.
                                <span class="sub-heading">SKU: 143600</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div> --}}
                {{-- <div data-product-name="Est recusandae eum.">
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 482.00</div>
                            <img class="md-card-head-img" src="assets/img/ecommerce/s6_edge_2.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Est recusandae eum.
                                <span class="sub-heading">SKU: 115166</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-small" href="ecommerce_product_details.html">More</a>
                        </div>
                    </div>
                </div> --}}
            </div>
             <div class="md-fab-wrapper">
                <a class="md-fab md-fab-accent" href="#snippet_new" data-uk-modal="{center:true,bgclose:false,modal:false}">
                 <i class="material-icons">add</i>
                </a>
             </div>
      <div class="uk-modal" id="snippet_new" >
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Upload a book</h3>
            </div>
                 {!! Form::open(['action'=>'BooksController@insert','method'=>'post','role'=>'form','enctype'=>'multipart/form-data']) !!}
                    <div class="uk-width-2-2">
                               <div class="parsley-row">
                                 <label>Text:</label>
                                    <textarea class="md-input" id="task_description" name="text"></textarea>
                               </div>
                               
                           </div>
                    <!-- IMAGE boxes -->
                                    <div class="uk-width-1-2 image" id="image">
                                           <div ><br>
                                           <!-- <button class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" onclick="iappendText()">ADD</button> -->
                                             </div>
                                       <div class="uk-width-medium-1-2">
                                            <div class="md-card">
                                             <div class="md-card-content">
                                                  <input type="file" id="input-file-a" class="dropify" name="file" />
                                                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                </div>
                                             </div>
                                        </div>
                                    </div>        
                 <div class="uk-modal-footer uk-text-right">
                           <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                           <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                 </div>
            {!! Form::close() !!}
        </div>
      </div>

        </div>
    </div>


   {{-- dabase record delete button   --}}

  



    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
    <script src="assets/js/jquery.min.js"></script>
    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>
    <script src="bower_components/d3/d3.min.js"></script>
    <!-- metrics graphics (charts) -->
    <script src="bower_components/metrics-graphics/dist/metricsgraphics.min.js"></script>
    <!-- c3.js (charts) -->
    <script src="bower_components/c3js-chart/c3.min.js"></script>
    <!-- chartist -->
    <script src="bower_components/chartist/dist/chartist.min.js"></script>

    <!--  charts functions -->
    <script src="assets/js/pages/plugins_charts.min.js"></script>

    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    <!-- droppify -->
     <script src="bower_components/dropify/dist/js/dropify.min.js"></script>
    <!-- parsley (validation) --> 
    <script>
    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    </script>
    <script src="bower_components/parsleyjs/dist/parsley.min.js"></script>
    

    <script>
        $(function() {
            if(isHighDensity()) {
                $.getScript( "assets/js/custom/dense.min.js", function(data) {
                    // enable hires images
                    altair_helpers.retina_images();
                });
            }
            if(Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });
        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
    </script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-65191727-1', 'auto');
        ga('send', 'pageview');
    </script>

    <div id="style_switcher">
        
       
        <div class="uk-visible-large uk-margin-medium-bottom">
            <h4 class="heading_c">Sidebar</h4>
            <p>
                <input type="checkbox" name="style_sidebar_mini" id="style_sidebar_mini" data-md-icheck />
                <label for="style_sidebar_mini" class="inline-label">Mini Sidebar</label>
            </p>
            <p>
                <input type="checkbox" name="style_sidebar_slim" id="style_sidebar_slim" data-md-icheck />
                <label for="style_sidebar_slim" class="inline-label">Slim Sidebar</label>
            </p>
        </div>
        <div class="uk-visible-large uk-margin-medium-bottom">
            <h4 class="heading_c">Layout</h4>
            <p>
                <input type="checkbox" name="style_layout_boxed" id="style_layout_boxed" data-md-icheck />
                <label for="style_layout_boxed" class="inline-label">Boxed layout</label>
            </p>
        </div>
        <div class="uk-visible-large">
            <h4 class="heading_c">Main menu accordion</h4>
            <p>
                <input type="checkbox" name="accordion_mode_main_menu" id="accordion_mode_main_menu" data-md-icheck />
                <label for="accordion_mode_main_menu" class="inline-label">Accordion mode</label>
            </p>
        </div>
    </div>

    <script>
         //Image input
        $(".dropify").dropify(
            {messages:{default:"Add file"}})

        $(function() {
            var $switcher = $('#style_switcher'),
                $switcher_toggle = $('#style_switcher_toggle'),
                $theme_switcher = $('#theme_switcher'),
                $mini_sidebar_toggle = $('#style_sidebar_mini'),
                $slim_sidebar_toggle = $('#style_sidebar_slim'),
                $boxed_layout_toggle = $('#style_layout_boxed'),
                $accordion_mode_toggle = $('#accordion_mode_main_menu'),
                $html = $('html'),
                $body = $('body');


            $switcher_toggle.click(function(e) {
                e.preventDefault();
                $switcher.toggleClass('switcher_active');
            });

            $theme_switcher.children('li').click(function(e) {
                e.preventDefault();
                var $this = $(this),
                    this_theme = $this.attr('data-app-theme');

                $theme_switcher.children('li').removeClass('active_theme');
                $(this).addClass('active_theme');
                $html
                    .removeClass('app_theme_a app_theme_b app_theme_c app_theme_d app_theme_e app_theme_f app_theme_g app_theme_h app_theme_i app_theme_dark')
                    .addClass(this_theme);

                if(this_theme == '') {
                    localStorage.removeItem('altair_theme');
                    $('#kendoCSS').attr('href','bower_components/kendo-ui/styles/kendo.material.min.css');
                } else {
                    localStorage.setItem("altair_theme", this_theme);
                    if(this_theme == 'app_theme_dark') {
                        $('#kendoCSS').attr('href','bower_components/kendo-ui/styles/kendo.materialblack.min.css')
                    } else {
                        $('#kendoCSS').attr('href','bower_components/kendo-ui/styles/kendo.material.min.css');
                    }
                }

            });

            // hide style switcher
            $document.on('click keyup', function(e) {
                if( $switcher.hasClass('switcher_active') ) {
                    if (
                        ( !$(e.target).closest($switcher).length )
                        || ( e.keyCode == 27 )
                    ) {
                        $switcher.removeClass('switcher_active');
                    }
                }
            });

            // get theme from local storage
            if(localStorage.getItem("altair_theme") !== null) {
                $theme_switcher.children('li[data-app-theme='+localStorage.getItem("altair_theme")+']').click();
            }


        // toggle mini sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_mini") !== null && localStorage.getItem("altair_sidebar_mini") == '1') || $body.hasClass('sidebar_mini')) {
                $mini_sidebar_toggle.iCheck('check');
            }

            $mini_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_mini", '1');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                });

        // toggle slim sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_slim") !== null && localStorage.getItem("altair_sidebar_slim") == '1') || $body.hasClass('sidebar_slim')) {
                $slim_sidebar_toggle.iCheck('check');
            }

            $slim_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_slim", '1');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                });

        // toggle boxed layout

            if((localStorage.getItem("altair_layout") !== null && localStorage.getItem("altair_layout") == 'boxed') || $body.hasClass('boxed_layout')) {
                $boxed_layout_toggle.iCheck('check');
                $body.addClass('boxed_layout');
                $(window).resize();
            }

            $boxed_layout_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_layout", 'boxed');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_layout');
                    location.reload(true);
                });

        // main menu accordion mode
            if($sidebar_main.hasClass('accordion_mode')) {
                $accordion_mode_toggle.iCheck('check');
            }

            $accordion_mode_toggle
                .on('ifChecked', function(){
                    $sidebar_main.addClass('accordion_mode');
                })
                .on('ifUnchecked', function(){
                    $sidebar_main.removeClass('accordion_mode');
                });


        });
    </script>

   <script type="text/javascript">

    // edit button

    $(document).on('click', '.edit', function(){
            var id = $(this).attr("id");
            $('#form_output').html('');
            $.ajax({
                url: {{route('books.fetchdata')}},
                method:'get',
                data:{id:id},
                dataType:'json',
                success:function(data)
                {
                    $('#text').val(data.text);
                    $('#file').val(data.file);
                    $('#book_id').val(id);
                    $('#bookModal').modal('show');
                    $('#action').val('Edit');
                    $('#button_action').val('update');
                }
            })
    });

    
    function updateBook(argument) {
        alert(argument);
    } 

    //delete book
     $.ajaxSetup({
        
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    

   // $(".delete").click(function(){
   //      var id = $(this).data("id");
   //      var token = $(this).data("token");
   //      alert(id)
   //      

   //      console.log("It failed");
   //  });
       
   
        // $('child_book').DataTable({
        //     "processing":true,
        //     "surverSide":true,
        //     "ajax": "",
        //     "columns":[
        //         {"data": "text"},
        //         {"data": "image"},
        //         {"data": "taxt"},
        //     ]
        // })

       // $(document).on('click' , '.delete', function(){
         // $('.delete').on('click',function(){
         //    var id = $(this).data('biid');
         //    $.get('{{URL::to('biid/delete/?id=')}}'+id,function(data){

         //    });
       //    var id = $(this).attr('id');
       //       if(confirm("Are you sure you want to Remove data with"+id+"?"))
       //       {
       //          $.ajax({

       //              method: "get",
       //              data: {id:id},
       //              success:function(data)
       //              {
       //                  alert(data);
       //                  $('#child_book').ajax.reload();
       //              }
       //          });
       //       }
       //       else
       //       {
       //          return false;
       //       }
       // });
  

   </script>
</body>

<!-- Mirrored from altair_html.tzdthemes.com/ecommerce_products_grid.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Feb 2019 11:15:27 GMT -->
</html>