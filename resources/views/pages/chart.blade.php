<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->


<!-- Mirrored from altair_html.tzdthemes.com/page_blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Feb 2019 11:17:38 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/images.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/images.png" sizes="32x32">

    <title>{{config('app.name','Data Collection')}}</title>
    <!-- additional styles for plugins -->
    <!-- dropify -->
    <link rel="stylesheet" href="assets/skins/dropify/css/dropify.css">
<!-- metrics graphics (charts) -->
    <link rel="stylesheet" href="bower_components/metrics-graphics/dist/metricsgraphics.css">
    <!-- c3.js (charts) -->
    <link rel="stylesheet" href="bower_components/c3js-chart/c3.min.css">
    <!-- chartist -->
    <link rel="stylesheet" href="bower_components/chartist/dist/chartist.min.css">

    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="assets/css/themes/themes_combined.min.css" media="all">
    <style type="text/css">
        .focused {
            background-color: #1976d2;

        }
        .focused a{
            color: #fff;

        }
        .pview{
            white-space: nowrap;
            width: 100%;
            border: 1px solid transparent;
            overflow: hidden;
            text-overflow: ellipsis;
        }
.righticon {
  position: relative;
  left: 220px;
        }
    </style>
    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
        <link rel="stylesheet" href="assets/css/ie.css" media="all">
    <![endif]-->

</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                                
                <!-- main sidebar switch -->
             
                
                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>
              
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        
                       
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                        <li class="uk-width-1-2 uk-active"><a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>
                                        <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>
                                    </ul>
                                    <ul id="header_alerts" class="uk-switcher uk-margin">
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-cyan">pj</span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Omnis consequatur ut.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Eligendi quo laboriosam explicabo perferendis consequatur soluta.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/capture.png" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Eveniet cumque.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Numquam adipisci aliquam consequatur sequi recusandae ducimus quia aliquid aspernatur eos qui.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-light-green">ce</span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Quod autem.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Explicabo optio nesciunt hic itaque qui.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/capture.png" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Nostrum quam.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Magni porro veniam consequatur vero accusantium.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_09_tn.png" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Culpa architecto rerum.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Sequi laboriosam id atque ipsam quia id.</span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                                            </div>
                                        </li>
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Nesciunt reprehenderit.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Est est occaecati aliquid delectus.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Exercitationem et.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Debitis ut voluptate voluptas velit provident nesciunt accusantium.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Voluptates reprehenderit.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Sit quas non odit quia quia.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Illum laboriosam.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">In dicta ea tempore modi ipsam.</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image" src="assets/img/avatars/mell.png" alt=""/></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                     <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a></li>
                                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form uk-autocomplete" data-uk-autocomplete="{source:'data/search_data.json'}">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
         
            </form>
        </div>
    </header><!-- main header end -->
    <!-- main sidebar -->
    <aside id="sidebar_main">
        
        <div class="sidebar_main_header">
            <div class="sidebar_logo">
                <a href="page_blank" class="sSidebar_hide sidebar_logo_large">
                    <img class="logo_regular" src="assets/img/unice.png" alt="" height="15" width="80"/>
                    <img class="logo_light" src="assets/img/logo_main_white.png" alt="" height="15" width="71"/>
                </a>
                <a href="index-2.html" class="sSidebar_show sidebar_logo_small">
                    <img class="logo_regular" src="assets/img/logo_main_small.png" alt="" height="32" width="32"/>
                    <img class="logo_light" src="assets/img/logo_main_small_light.png" alt="" height="32" width="32"/>
                </a>
            </div>
            
        </div>
        
        <div class="menu_section">
            <ul>
                <li title="Dashboard">
                    <a href="dashboard">
                        <span class="menu_icon"><i class="material-icons">&#xE85C</i></span>
                        <span class="menu_title">Dashboard</span>
                    </a>
                    
               </li>
                
              
                <li title="Pages">
                    <a href="page_blank">
                        <span class="menu_icon"><i class="material-icons">&#xE8D2;</i></span>
                        <span class="menu_title">Questions</span>
                    </a>
                </li>
                <li title="Pages">
                    <a href="books">
                        <span class="menu_icon"><i class="material-icons">&#xE24D;</i></span>
                        <span class="menu_title">Child protect message</span>
                    </a>
                </li>
                         
            </ul>
        </div>
    </aside><!-- main sidebar end -->


<div id="page_content">
    <div id="page_content_inner">
   



            <!-- chart part on page -->

       <div class="uk-width-large-2-3">
                   <div class="md-card">
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">Answers Statistics</h4>
                            <div id="chartist_distributed_series1" class="chartist"></div>
                        </div>
                    </div>
               
            </div>
    </div>
</div>

<script>
     function provideContent(){
       var checkboxValue = document.getElementById('select_demo_5').value;
           // alert(checkboxValue);
           if(checkboxValue == 'number')
           {
              $(".number").removeAttr("hidden");
              $(".number").removeClass("uk-hidden");
           }

           else if(checkboxValue == 'closed')
           {
              $(".closed").removeAttr("hidden");
              $(".closed").removeClass("uk-hidden");
           }
           else if(checkboxValue == 'open')
           {
           
              $(".open").removeAttr("hidden");
              $(".open").removeClass("uk-hidden");
           }
           else if(checkboxValue == 'image')
           {
               
              $(".image").removeAttr("hidden");
              $(".image").removeClass("uk-hidden");
           }

                                 }
</script>


<!-- addition  for option-->
<script>
 function oappendText() {
    var optionsNumber = Number($("#optionsNumber").val());
    var newoptionsNumber = optionsNumber+1;
    var currentValue = $("#optionInput["+optionsNumber+"]").val();
    var txt1 = '<input type="text" class="md-input" min="1" max="12" placeholder="Option" value="" name="optionInput['+newoptionsNumber+']" id="optionInput['+newoptionsNumber+']">';

    //empty the currebt value
    $("#optionInput").val("");

    if( currentValue == ""){
      alert("option must be filled out");
      return false;
    }
    
    // Create text with HTML
    // Create text with DOM
    $("#add").append(txt1);   // Append new elements
    $("#optionsNumber").val(Number(newoptionsNumber));   // Append new elements
 }
</script>


<!-- addition for check box -->
<script>
 function cappendText() {
    var currentValue = $("#checkInput").val();
    var txt1 = '<input type="text" class="md-input"  min="1" max="12" placeholder="Option" value="'+currentValue+'">';

    //empty the currebt value
    $("#checkInput").val("");

    if( currentValue == ""){
      alert("this question must be filled out");
      return false;
    }
    
    // Create text with HTML
    // Create text with DOM
    $("#check").append(txt1);   // Append new elements
 }
</script>

<!-- addition for image box -->
{{-- <script>
 function iappendText() {
    var currentValue = $("#imageInput").val();
    var txt1 = '<input type="text" class="md-input"  min="1" max="12" placeholder="Option" value="'+currentValue+'">';

    //empty the currebt value
    $("#imageInput").val("");

    if( currentValue == ""){
      alert("imagebox must be filled out");
      return false;
    }
    
    // Create text with HTML
    // Create text with DOM
    $("#image").append(txt1);   // Append new elements
 }
</script> --}}





    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>


    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>
   <!-- page specific plugins -->
    <!-- d3 -->
    <script src="bower_components/d3/d3.min.js"></script>
    <!-- metrics graphics (charts) -->
    <script src="bower_components/metrics-graphics/dist/metricsgraphics.min.js"></script>
    <!-- c3.js (charts) -->
    <script src="bower_components/c3js-chart/c3.min.js"></script>
    <!-- chartist -->
    <script src="bower_components/chartist/dist/chartist.min.js"></script>

    <!--  charts functions -->
    <script src="assets/js/pages/plugins_charts.min.js"></script>

    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    <!-- droppify -->
     <script src="bower_components/dropify/dist/js/dropify.min.js"></script>
    <!-- parsley (validation) -->
    <script>
    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    </script>
    <script src="bower_components/parsleyjs/dist/parsley.min.js"></script>

     <script type="text/javascript">
            $('.select-item').on('click',function(){
             
                var selectId = $(this).data('value');
                console.log(data);
                
             
            })
        </script>
    

     <script type="text/javascript">
            $('.read-data').on('click',function(){

                //take the service ID
                // var select = $("#itemInvoiceCode").val();   
                var serviceId = $(this).data('service');

                $.get('{{URL::to('Service/read-data/?id=')}}'+serviceId,function(data){
                    // console.log(data);
                
                $('#service-info').empty().html(data);
                   

                })
            })
        </script>
    
    
{{-- ajax --}}
<script src="assets/js/ajax.js"></script>


<script>
        $(function() {
            if(isHighDensity()) {
                $.getScript( "assets/js/custom/dense.min.js", function(data) {
                    // enable hires images
                    altair_helpers.retina_images();
                });
            }
            if(Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });
        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
    </script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-65191727-1', 'auto');
        ga('send', 'pageview');
    </script>

   
    <script>

        //Toggle answers
        $(".service-item").on('click', function(){

            //remove all elements
            var serviceItems = $(".service-item")
            for (var i = serviceItems.length - 1; i >= 0; i--) {
                $(serviceItems[i]).removeClass('focused')
            }

            console.log($(this))

            $(this).addClass("focused");
        })

        $(".pview").hover(function(){
            //remove all elements
            $(this).removeClass("pview");
        }, function(){
            $(this).addClass("pview");
        });
          // image input
        $(".dropify").dropify(
            {messages:{default:"Add image"}})
        $(function() {
            var $switcher = $('#style_switcher'),
                $switcher_toggle = $('#style_switcher_toggle'),
                $theme_switcher = $('#theme_switcher'),
                $mini_sidebar_toggle = $('#style_sidebar_mini'),
                $slim_sidebar_toggle = $('#style_sidebar_slim'),
                $boxed_layout_toggle = $('#style_layout_boxed'),
                $accordion_mode_toggle = $('#accordion_mode_main_menu'),
                $html = $('html'),
                $body = $('body');


            $switcher_toggle.click(function(e) {
                e.preventDefault();
                $switcher.toggleClass('switcher_active');
            });

            $theme_switcher.children('li').click(function(e) {
                e.preventDefault();
                var $this = $(this),
                    this_theme = $this.attr('data-app-theme');

                $theme_switcher.children('li').removeClass('active_theme');
                $(this).addClass('active_theme');
                $html
                    .removeClass('app_theme_a app_theme_b app_theme_c app_theme_d app_theme_e app_theme_f app_theme_g app_theme_h app_theme_i app_theme_dark')
                    .addClass(this_theme);

                if(this_theme == '') {
                    localStorage.removeItem('altair_theme');
                    $('#kendoCSS').attr('href','bower_components/kendo-ui/styles/kendo.material.min.css');
                } else {
                    localStorage.setItem("altair_theme", this_theme);
                    if(this_theme == 'app_theme_dark') {
                        $('#kendoCSS').attr('href','bower_components/kendo-ui/styles/kendo.materialblack.min.css')
                    } else {
                        $('#kendoCSS').attr('href','bower_components/kendo-ui/styles/kendo.material.min.css');
                    }
                }

            });

            // hide style switcher
            $document.on('click keyup', function(e) {
                if( $switcher.hasClass('switcher_active') ) {
                    if (
                        ( !$(e.target).closest($switcher).length )
                        || ( e.keyCode == 27 )
                    ) {
                        $switcher.removeClass('switcher_active');
                    }
                }
            });

            // get theme from local storage
            if(localStorage.getItem("altair_theme") !== null) {
                $theme_switcher.children('li[data-app-theme='+localStorage.getItem("altair_theme")+']').click();
            }


        // toggle mini sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_mini") !== null && localStorage.getItem("altair_sidebar_mini") == '1') || $body.hasClass('sidebar_mini')) {
                $mini_sidebar_toggle.iCheck('check');
            }

            $mini_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_mini", '1');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                });

        // toggle slim sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_slim") !== null && localStorage.getItem("altair_sidebar_slim") == '1') || $body.hasClass('sidebar_slim')) {
                $slim_sidebar_toggle.iCheck('check');
            }

            $slim_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_slim", '1');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                });

        // toggle boxed layout

            if((localStorage.getItem("altair_layout") !== null && localStorage.getItem("altair_layout") == 'boxed') || $body.hasClass('boxed_layout')) {
                $boxed_layout_toggle.iCheck('check');
                $body.addClass('boxed_layout');
                $(window).resize();
            }

            $boxed_layout_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_layout", 'boxed');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_layout');
                    location.reload(true);
                });

        // main menu accordion mode
            if($sidebar_main.hasClass('accordion_mode')) {
                $accordion_mode_toggle.iCheck('check');
            }

            $accordion_mode_toggle
                .on('ifChecked', function(){
                    $sidebar_main.addClass('accordion_mode');
                })
                .on('ifUnchecked', function(){
                    $sidebar_main.removeClass('accordion_mode');
                });


        });

        var a = new Chartist.Bar("#chartist_distributed_series1",{
            labels: ["BOYS", "GIRLS"],
            series: [70, 60],
            width: 500
        },{
            distributeSeries: !0
        });

    </script>

    {{-- ajax --}}
    @yield('script')


    {{--    <script  src="../../assets/js/jquery.min.js"></script>
       <script type="text/javascript">
           function getRequest(){
                alert("texe");
            }
       </script> --}}


</body>

<!-- Mirrored from altair_html.tzdthemes.com/page_blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Feb 2019 11:17:38 GMT -->
</html>