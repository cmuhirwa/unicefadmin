@extends('layouts.app')

@section('content')

<div class="jumbotron">
  <h1 class="display-4">Create your Post</h1>
  {!! Form::open(['action' => 'PostsController@store', 'method'=> 'POST']) !!}

  <div class="form-group">
   {{Form::label('title','Title')}}
   {{Form::text('title','',['class' => 'form-control', 'placeholder'=>'Title'])}}
  </div>
  <div class="form-group">
      {{Form::label('body','Body')}}
      {{Form::textarea('body','',['class' => 'form-control', 'placeholder'=>'Make your  Body Text'])}}
     </div>
     {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
     {!! Form::close() !!}
</div>
@endsection  