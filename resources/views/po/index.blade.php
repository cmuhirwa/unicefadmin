@extends('layouts.app')

@section('content')
<div class="jumbotron">
  <h1 class="display-4">The posts on App</h1><br><br>
  @if(count($posts) > 0)
     @foreach($posts as $post)
       <div class="well">
       <h2><a href="/posts/{{$post->id}}"> {{$post->title}}</a></h2>
        <small>Submitten on{{$post->created_at}}  </small>
       </div>
     @endforeach
     {{$posts->links()}}
     @else
      <h3>Mostsly successfull</h3>
  @endif
</div>
@endsection  