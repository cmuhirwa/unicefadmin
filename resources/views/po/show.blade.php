@extends('layouts.app')

@section('content')
<div class="jumbotron">
  <h1 class="display-4">{{$post->title}}</h1><br><br>
  <div>
    {{$post->body}}
  </div><br><br>
   <small>Submitted on{{$post->created_at}}</small><br><br>
   <a href="/posts" class="btn btn-primary">Go Back</a>
</div>
@endsection  