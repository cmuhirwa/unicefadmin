<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

       
     <link rel="stylesheet" type="text/css" href="../../assets/css/main.min.css">
       
    </head>
    <body>
       <div>
           <h1>Get Request</h1>
           <button type="button" class="btn btn-primary"  id="getRequest">Get Request</button>
       </div>
       <div id="getRequestData">
           
       </div>
       <script  src="../../assets/js/jquery.min.js"></script>
       
    </body>
</html>
