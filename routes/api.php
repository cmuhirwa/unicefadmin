<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::delete('/books/{bid}','BooksDeleteController@destroy');
// Route::resource('/books/{id}','BookUpdateController');
// Route::put('/books/{id}', 'BookUpdateController@store');
// Route::delete('/books/{id}', 'BooksDeleteController@destroy');


// Route::get('/page_blank','PagesController@page_blank');
// Route::delete('/page_blank/{id}','PagesController@destroy');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('my_first_api', 'HomeController@my_first_api');
Route::post('/books', 'BooksController@insert');
Route::get('/books', 'BooksController@product');
Route::post('/page_blank','PagesController@save');
Route::post('/dashboard','PagesController@insert');
Route::post('/page_blank','PagesController@page_blank');