<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/', function () {
//      return view('pages.login');
   
// });
Route::get('/', function () {
    return view('pages.login');
});

// Route::get('/getRequest', function(){
// 	if(Request::ajax()){
// 		return 'the request has loaded';
// 	}
// });
Route::get('/dashboard', 'PagesController@dashboard'); 
Route::get('/index', 'PagesController@index');
Route::get('/page_blank','PagesController@page_blank');
// Route::get('/page_blank','PagesController@answer');
Route::get('/page_blank/excel','PagesController@excel')->name('page_blank.excel');
Route::get('remove/{id}','PagesController@destroy');
Route::post('/page_blank','PagesController@save');
// Route::get('/page_blank','ChartController@SessionAnswer');	
Route::post('/dashboard','PagesController@insert');
Route::get('/question', 'QuestionController@question');
Route::post('/question', 'QuestionController@save');

Route::get('/chat', 'ChartController@SessionAnswer');
// Route::get('/chat', 'ChartController@example');

Route::resource('/posts', 'PostsController');
Route::get('posts/create', 'PostsController@create');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get("/piechart", "QuestionController@Piechart");

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/Service/read-data', 'AjaxController@readData');
Route::get('/books', 'BooksController@product');
Route::get('delete/{id}','BooksDeleteController@destroy');
// Route::delete('/books/{id}', 'BooksDeleteController@destroy');
Route::resource('/books/{id}','BookUpdateController');
Route::resource('/page_blank/{id}','QuestionController');
// Route::get('/books/removedata/{id}','BooksDeleteController@removedata')->name('books.removedata');
// Route::post('/books', 'BooksDeleteController@updateBook');
Route::post('/books', 'BooksController@insert');
Route::get('books/fetchdata', 'BooksController@fetchdata')->name('books.fetchdata');
// Route::get('/forms_dynamic' 'ServiceController@forms_dynamic')

// Route::get('/message', function () {
//      return view('message');
   
// });

// Route::get('/message', 'MessageController@index')->name('message');

